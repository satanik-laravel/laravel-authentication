# Satanik Authentication Package

[![Software License][ico-license]](LICENSE.md)

This package adds a user base class and routes to perform JWT authentication.

### Dependencies

* satanik/api - [git@gitlab.com:satanik/laravel-api.git]()

## Structure

The directory structure follows the industry standard.

```bash
config/
src/
tests/
```


## Install

Via Composer

``` bash
$ composer require satanik/authentication
```

if the package is not published use this in the composer file

```json
"repositories": {
  "satanik/authentication": {
    "type": "vcs",
    "url": "git@gitlab.com:satanik/laravel-authentication.git"
  },
  ...
},
...
"require": {
  ...
  "satanik/authentication": "<version constraint>",
```

or copy the repository to

```bash
<project root>/packages/Satanik/Authentication
```

and use this code in the composer file

```json
"repositories": {
  "satanik/authentication": {
    "type": "path",
    "url": "packages/Satanik/Authentication",
    "options": {
      "symlink": true
    }
  },
  ...
},
...
"require": {
  ...
  "satanik/authentication": "@dev",
```

For the package to be usable the migrations have to be applied as well with

```bash
php artisan migrate
```

## Usage

### Authentication

The `Authentication` facade provides two functions, one to create the routes for authentiction, the other for adding a user creation factory. To use them add the following code in

```bash
routes/api.php
```

```php
Authentication::routes();
```

and furthermore in

```bash
database/factories/UserFactory.php
```

```php
Authentication::factories($factory);
```

The following routes are then produced

```php
Route::group([
    'prefix' => 'user',
    'namespace' => '\Satanik\Authentication\Controllers',
    'middleware' => ['satanik.api.intercept']
], function() {
   Route::post('login', 'UsersController@login');
   Route::post('logout', 'UsersController@logout');
   Route::put('update', 'UsersController@update');
   Route::get('me', 'UsersController@me');
   Route::post('refresh', 'UsersController@refresh');
   Route::get('verify', 'UsersController@verify')->name('verification.verify');
});
```

### Admin

There is also a middleware that can be added to routes to ensure only users flagged with admin are able to access them. This is mainly useful for managment routes e.g. creating the api docs etc.

To add this middleware use the middleware string `satanik.admin`.

The admin flag has to be set manually in the database, as there is no means to do it from the application yet.

## Testing

``` bash
$ composer test
```

## Security

If you discover any security related issues, please email daniel@satanik.at instead of using the issue tracker.

## Credits

- [Daniel Satanik][link-author]

## License

The MIT License (MIT). Please see [License File](LICENSE.md) for more information.

[ico-license]: https://img.shields.io/badge/license-MIT-brightgreen.svg?style=flat-square

[link-author]: https://satanik.at
