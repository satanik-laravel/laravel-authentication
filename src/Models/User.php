<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 05.04.18
 * Time: 17:12
 */

namespace Satanik\Authentication\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Satanik\Authentication\Concerns\MustVerifyEmail as MustVerifyEmailConcern;
use Satanik\Foundation\Abstraction\User as Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Satanik\Authentication\Models\User
 *
 * @property int $id
 * @property string $class
 * @property int|null $role_id
 * @property string|null $username
 * @property string|null $name
 * @property string $email
 * @property string|null $email_verified_at
 * @property string $password
 * @property string|null $remember_token
 * @property \Illuminate\Support\Carbon|null $created_at
 * @property \Illuminate\Support\Carbon|null $updated_at
 * @property string|null $description
 * @property string|null $uid
 * @property string|null $profile_image
 * @property string|null $sector
 * @property string|null $address
 * @property string|null $phone_number
 * @property string|null $fax_number
 * @property mixed|null $services
 * @property-read false|string $token
 * @property-read \Illuminate\Notifications\DatabaseNotificationCollection|\Illuminate\Notifications\DatabaseNotification[] $notifications
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\User joinModel($model, $one, $operator = null, $two = null, $type = 'inner', $where = false)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\User joinPivot($table)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\User nPerGroup($group, $number = 10)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Foundation\Abstraction\User top($number = 10)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereAddress($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereClass($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereCreatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereDescription($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereEmail($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereEmailVerifiedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereFaxNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereName($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User wherePassword($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User wherePhoneNumber($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereProfileImage($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereRememberToken($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereRoleId($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereSector($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereServices($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereUid($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereUpdatedAt($value)
 * @method static \Illuminate\Database\Eloquent\Builder|\Satanik\Authentication\Models\User whereUsername($value)
 * @mixin \Eloquent
 */
class User extends Authenticatable implements MustVerifyEmail, JWTSubject
{
    use MustVerifyEmailConcern;

    protected $appends = ['token'];

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims()
    {
        return [];
    }

    /**
     * @return false|string
     */
    public function getTokenAttribute()
    {
        return \JWTAuth::fromUser($this);
    }
}
