<?php

namespace Satanik\Authentication\Concerns;

use Satanik\Authentication\Notifications\VerifyEmail;

trait MustVerifyEmail
{
    use \Illuminate\Auth\MustVerifyEmail {
        sendEmailVerificationNotification as _send;
    }

    public function sendEmailVerificationNotification(): void
    {
        $this->notify(new VerifyEmail);
    }
}
