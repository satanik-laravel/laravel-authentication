<?php

namespace Satanik\Authentication;

use App;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factory;
use Illuminate\Support\Str;
use Satanik\Authentication\Classes\UserRole;
use Satanik\Authentication\Models\User;

class Authentication
{
    public function routes(): void
    {
        if (!App::routesAreCached()) {
            require __DIR__ . '/routes.php';
        }
    }

    public function factories(Factory $factory): void
    {
        $factory->define(User::bound(), function (\Faker\Generator $faker) {
            $faker = \Faker\Factory::create('de_AT');

            $transliterate = function ($string) {
                $string = strtolower(Str::transliterate($string));
                return $string;
            };

            $username = function ($first, $last) use ($transliterate) {
                return $transliterate(ucfirst($first) . '.' . ucfirst($last));
            };

            do {
                $first = $faker->firstName;
                $last  = $faker->lastName;
                $name  = $username($first, $last);
            } while (User::where('username', $name)->exists());

            return [
                'username'          => $name,
                'email'             => function (array $user) use ($faker) {
                    return "{$user['username']}@" . $faker->safeEmailDomain;
                },
                'password'          => $faker->password,
                'email_verified_at' => Carbon::now(),
                'class'             => UserRole::DEFAULT,
            ];
        });

        $factory->state(User::bound(), 'admin', [
            'class' => UserRole::ADMIN,
        ]);
    }
}
