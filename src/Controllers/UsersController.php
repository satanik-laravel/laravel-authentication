<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 31.12.17
 * Time: 16:02
 */

namespace Satanik\Authentication\Controllers;

use Assert;
use Auth;
use DB;
use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Events\Verified;
use Illuminate\Http\Request;
use Satanik\Authentication\Models\User;
use Satanik\Authentication\Requests\CreateUserRequest;
use Satanik\Authentication\Requests\UpdateProfileRequest;
use Satanik\Foundation\Abstraction\Controller;
use Satanik\Foundation\Abstraction\UserResource;

class UsersController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api')->except(['login']);
        $this->middleware('verified')->except(['login', 'logout', 'verify', 'resend']);
    }

    /**
     * @param \Satanik\Authentication\Requests\CreateUserRequest $request
     *
     * @return array
     * @throws \Satanik\Exceptions\Types\Exception
     */
    public function login(CreateUserRequest $request): array
    {
        $key = $request->input('key');
        if ($request->input('login')) {
            $field = filter_var($key, FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
            $request->merge([$field => $key]);
            $credentials = $request->only($field, 'password');

            $user = Assert::login($credentials);
        } else {
            $request->merge(['email' => $key]);
            $model       = User::bound();
            $credentials = $request->only('email', 'password');
            $user        = $model::create($credentials);
            $user        = $model::find($user->id);

            event(new Registered($user));
        }
        return ["key" => "user", "data" => UserResource::make($user)];
    }

    public function logout(): void
    {
        Auth::logout();
    }

    /**
     * @param UpdateProfileRequest $request
     *
     * @return array
     * @throws \Exception
     * @throws \Throwable
     */
    public function update(UpdateProfileRequest $request): array
    {
        $input = $request->only(array_keys(Auth::user()->attributesToArray()));
        $input = collect($input)
            ->filter(function ($v) {
                return !is_string($v) || trim($v) !== '';
            })
            ->map(function ($v) {
                if (\is_array($v)) {
                    return json_encode($v);
                }
                return $v;
            })->all();
        return DB::transaction(function () use ($input) {
            Auth::user()->update($input);
            return ['key' => 'user', 'data' => UserResource::make(Auth::user())];
        });
    }

    public function me(): array
    {
        return ["key" => "user", "data" => UserResource::make(Auth::user())];
    }

    public function refresh(): array
    {
        Auth::refresh();
        return ["key" => "user", "data" => UserResource::make(Auth::user())];
    }

    public function verify(Request $request): array
    {
        if ($request->user()->markEmailAsVerified()) {
            event(new Verified($request->user()));
        }

        return ["key" => "user", "data" => UserResource::make($request->user())];
    }

    public function resend(Request $request): ?array
    {
        if ($request->user()->hasVerifiedEmail()) {
            return ["key" => "user", "data" => UserResource::make($request->user())];
        }

        $request->user()->sendEmailVerificationNotification();
        return null;
    }
}
