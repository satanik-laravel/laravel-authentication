<?php

namespace Satanik\Authentication\Middleware;

use Closure;
use Illuminate\Http\Request;
use Satanik\Exceptions\Types\Exception;

class Admin
{
    /**
     * Handle an incoming request.
     *
     * @param Request   $request
     * @param  \Closure $next
     *
     * @return mixed
     * @throws Exception
     */
    public function handle(Request $request, Closure $next)
    {
        \Assert::admin($request->user());

        return $next($request);
    }
}
