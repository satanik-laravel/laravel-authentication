<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 06.04.18
 * Time: 14:34
 */

namespace Satanik\Authentication\Requests;

use Illuminate\Support\Str;
use Illuminate\Validation\Validator;
use Satanik\Authentication\Models\User;
use Satanik\Foundation\Abstraction\Request;

class CreateUserRequest extends Request
{
    public function rules(): array
    {
        return [
            'key'      => 'required|string',
            'password' => 'required|string|min:6',
        ];
    }

    /**
     * @param \Illuminate\Validation\Validator $validator
     */
    public function withValidator(Validator $validator): void
    {
        $validator->sometimes('key', 'exists:users,username', function ($data) {
            return !(Str::transliterate($data->key) == $data->key && filter_var($data->key,
                    FILTER_VALIDATE_EMAIL));
        });
        $validator->sometimes('key', 'email', function ($data) {
            return Str::transliterate($data->key) == $data->key && filter_var($data->key,
                    FILTER_VALIDATE_EMAIL);
        });
        $validator->setAttributeNames(['key' => trans('satanik::attributes.key')]);
    }

    /**
     * Get all of the input and files for the request.
     *
     * @param  array|mixed $keys
     *
     * @return array
     */
    public function all($keys = null): array
    {
        $input = parent::all($keys);
        if (isset($input['key'])) {
            $input['key']   = trim($input['key']);
            $user           = User::bound();
            $input['login'] = !filter_var($input['key'], FILTER_VALIDATE_EMAIL) ||
                $user::where('email', $input['key'])->exists();
            $this->merge(['login' => $input['login']]);
        }
        return $input;
    }
}
