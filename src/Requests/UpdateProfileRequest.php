<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 31.12.17
 * Time: 16:04
 */

namespace Satanik\Authentication\Requests;

use Auth;
use Carbon\Carbon;
use Satanik\Foundation\Abstraction\Request;

class UpdateProfileRequest extends Request
{
    public function rules(): array
    {
        $attributes = array_keys(Auth::user()->attributesToArray());
        $table      = Auth::user()->getTable();
        $columns    = \DB::select(\DB::raw("SHOW COLUMNS FROM `{$table}`"));
        $columns    = collect($columns)->filter(function ($value) use ($attributes) {
            return in_array($value->Field, $attributes) && (empty($value->Key) || $value->Key !== 'PRI');
        });

        $validation = [];

        $mappings = collect([
            'varchar'  => 'string',
            'text'     => 'string',
            'int'      => 'integer',
            'smallint' => 'integer',
            'date'     => 'date',
            'json'     => 'array',
        ]);

        $specials = collect([
            'email'    => 'email',
            'homepage' => 'url',
        ]);

        foreach ($columns as $column) {
            $name = $column->Field;
            $type = $column->Type;
            $key  = $column->Key;
            if (preg_match('/(?<type>\w+)(?:\((?<size>\d+)\))?(?: (?<mod>\w+))?/', $type, $matches)) {
                $validation[$name] = 'nullable';
                if ($specials->has($name)) {
                    $validation[$name] .= '|' . $specials[$name];
                } else {
                    if ($mappings->has($matches['type'])) {
                        $validation[$name] .= '|' . $mappings[$matches['type']];
                    }
                }
                if (isset($matches['size'])) {
                    $validation[$name] .= '|max:' . $matches['size'];
                }
                if (isset($matches['mod']) && $matches['mod'] === 'unsigned') {
                    $validation[$name] .= '|min:0';
                }
                if ($key === 'UNI') {
                    $validation[$name] .= '|unique:users,' . $name;
                }
                if ($name === 'birthdate') {
                    $validation[$name] .= '|before:' . Carbon::tomorrow()->subYears(18)->toDateString();
                }
            }
        }
        return $validation;
    }

    /**
     * Get all of the input and files for the request.
     *
     * @param  array|mixed $keys
     *
     * @return array
     */
    public function all($keys = null): array
    {
        $input = collect(parent::all($keys));
        $input = $input->reject(function ($value, $key) {
            return !key_exists($key, Auth::user()->attributesToArray()) || $value === Auth::user()->$key;
        });
        return $input->all();
    }
}
