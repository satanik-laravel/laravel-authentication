<?php

namespace Satanik\Authentication;

use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Satanik\Authentication\Middleware\Admin;
use Satanik\Authentication\Models\User;

class AuthenticationServiceProvider extends ServiceProvider
{
    /**
     * Perform post-registration booting of services.
     *
     * @param \Illuminate\Routing\Router $router
     *
     * @return void
     */
    public function boot(Router $router): void
    {
        if (app('env') == 'testing') {
            $this->loadRoutesFrom(__DIR__ . '/routes.php');
        }

        $this->loadMigrationsFrom(__DIR__ . '/../database/migrations');

        $router->aliasMiddleware('satanik.admin', Admin::class);

        $this->defineAssertMacros();
    }

    /**
     * Register any package services.
     *
     * @return void
     */
    public function register(): void
    {
        // Register the service the package provides.
        $this->app->singleton('satanik-authentication', Authentication::class);
    }

    private function defineAssertMacros(): void
    {
        /**
         * @param \Satanik\Authentication\Models\User $user
         *
         * @throws \Satanik\Exceptions\Types\Exception
         */
        \Assert::macro('admin', function (User $user): void {
            \Assert::authorized($user, function ($user) {
                return $user->class === 'admin';
            });
        });
    }
}
