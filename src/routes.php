<?php

Route::group([
    'prefix' => 'user',
    'namespace' => '\Satanik\Authentication\Controllers',
    'middleware' => ['satanik.api.intercept']
], function() {
   Route::post('login', 'UsersController@login');
   Route::post('logout', 'UsersController@logout');
   Route::put('update', 'UsersController@update');
   Route::get('me', 'UsersController@me');
   Route::post('refresh', 'UsersController@refresh');
   Route::get('verify', 'UsersController@verify')->name('verification.verify');
    Route::get('verify/resend', 'UsersController@resend')->name('verification.resend');
});
