<?php

namespace Satanik\Authentication\Classes;

use Satanik\Foundation\Abstraction\Enum;

class UserRole extends Enum
{
    public const DEFAULT = 'user';
    public const USER    = 'user';
    public const ADMIN   = 'admin';
}
