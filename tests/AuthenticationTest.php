<?php
/**
 * Created by PhpStorm.
 * User: satanik
 * Date: 09.04.18
 * Time: 23:59
 */

namespace Satanik\Foundation\Tests;

use Illuminate\Foundation\Testing\WithFaker;
use Satanik\Authentication\Models\User;

class AuthenticationTest extends TestCase
{
    use WithFaker;

    /**
     * Setup the test environment.
     *
     * @return void
     * @throws \Exception
     * @throws \Throwable
     */
    protected function setUp(): void
    {
        parent::setUp();
        $this->faker('de_AT');
    }

    protected function initialiseUser(): void
    {
        $this->user = factory(User::bound())->create([
            'email_verified_at' => null
        ]);
        $this->user_token = $this->user->token;
    }


    public function testCreateUserWithEmailSucceeds(): void
    {
        $response = $this->post('user/login', [
            'key'      => $this->faker->safeEmail,
            'password' => $this->faker->password(8),
        ]);

        $response->assertJson([
            'error' => 0,
        ]);
        $response->assertSuccessful();
    }

    public function testCreateUserWithNicknameFails(): void
    {
        $response = $this->post('user/login', [
            'key'      => $this->faker->userName,
            'password' => $this->faker->password(8),
        ]);

        $response->assertJson([
            'error'   => 10005,
            'messages' => [trans('validation.exists', ['attribute' => trans('satanik::attributes.key')])],
        ]);
        $response->assertStatus(400);
    }

    public function testLoginWithShortPasswordFails(): void
    {
        $response = $this->post('user/login', [
            'key'      => $this->faker->email,
            'password' => '12345',
        ]);

        $response->assertJson([
            'error' => 10005,
        ]);
        $response->assertStatus(400);
    }

    public function testLoginWithMissingFails(): void
    {
        $response = $this->post('user/login', []);

        $response->assertJson([
            'error' => 10005,
        ]);
        $response->assertStatus(400);
    }

    public function testLoginWithWrongCredentialsFails(): void
    {
        $user = factory(User::bound())->create([
            'password' => 'bigsecret',
        ]);

        $response = $this->post('user/login', [
            'key'      => $user->email,
            'password' => 'foobar',
        ]);

        $response->assertJson([
            'error' => 10025,
        ]);
        $response->assertStatus(400);
    }

    public function testLoginWithEmailSucceeds(): void
    {
        $user = factory(User::bound())->create([
            'password' => 'bigsecret',
        ]);

        $response = $this->post('user/login', [
            'key'      => $user->email,
            'password' => 'bigsecret',
        ]);

        $response->assertJson([
            'error' => 0,
        ]);
        $response->assertSuccessful();
    }

    public function testLoginWithNicknameSucceeds(): void
    {
        $user = factory(User::bound())->create([
            'password' => 'bigsecret',
        ]);

        $response = $this->post('user/login', [
            'key'      => $user->username,
            'password' => 'bigsecret',
        ]);

        $response->assertJson([
            'error' => 0,
        ]);
        $response->assertSuccessful();
    }
}
